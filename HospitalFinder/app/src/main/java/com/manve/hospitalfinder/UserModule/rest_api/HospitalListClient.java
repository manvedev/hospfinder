package com.manve.hospitalfinder.UserModule.rest_api;

import com.manve.hospitalfinder.UserModule.models.DetailResult;
import com.manve.hospitalfinder.UserModule.models.DistanceResult;
import com.manve.hospitalfinder.UserModule.models.PlaceList;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

import java.util.Map;

/**
 * Created by Mayank Gupta on 11-12-2017.
 */

public interface HospitalListClient {

    @GET("place/nearbysearch/json")
    Call<PlaceList> getNearbyHospitals(@QueryMap Map<String, String> params);

    @GET("distancematrix/json")
    Call<DistanceResult> getHospitalDistances(@QueryMap Map<String, String> params);

    @GET("place/details/json")
    Call<DetailResult> getHospitalDetails(@QueryMap Map<String, String> params);
}
