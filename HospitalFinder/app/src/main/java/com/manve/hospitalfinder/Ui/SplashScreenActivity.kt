package com.manve.hospitalfinder.Ui

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

import com.manve.hospitalfinder.UserModule.activities.UserActivity
import android.content.pm.PackageManager
import android.content.pm.PackageInfo
import android.net.Uri
import android.provider.Settings
import com.manve.hospitalfinder.GpsUtils
import com.manve.hospitalfinder.AppConstants
import android.app.Activity
import com.manve.hospitalfinder.AdminModule.AdminActivity
import com.manve.hospitalfinder.Login.LoginActivity


class SplashScreenActivity : AppCompatActivity() {

    private var isGPS = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.manve.hospitalfinder.R.layout.activity_splash_screen)
        var user:FirebaseUser? = FirebaseAuth.getInstance().currentUser
        GpsUtils(this).turnGPSOn { isGPSEnable ->
            // turn on GPS
            isGPS = isGPSEnable
        }
        val background = object :Thread(){
            override fun run() {
                try {
                    Thread.sleep(2000)
                    canToggleGPS()
                    turnGPSOn()
                    if(user!=null){
                        val uid = FirebaseAuth.getInstance().currentUser!!.uid
                        val rootRef = FirebaseDatabase.getInstance().reference
                        val uidRef = rootRef.child("Users").child(uid)
                        val valueEventListener = object : ValueEventListener {
                            override fun onDataChange(dataSnapshot: DataSnapshot) {
                                if (dataSnapshot.child("type").getValue(String::class.java) == "user") {
                                    startActivity(Intent(this@SplashScreenActivity, UserActivity::class.java))
                                } else if (dataSnapshot.child("type").getValue(String::class.java) == "hospital") {
                                    startActivity(Intent(this@SplashScreenActivity, AdminActivity::class.java))
                                }
                            }

                            override fun onCancelled(databaseError: DatabaseError) {
                                Log.d("FragmentActivity", databaseError.message)
                            }
                        }
                        uidRef.addListenerForSingleValueEvent(valueEventListener)

                    }
                    else{
                        startActivity(Intent(this@SplashScreenActivity, LoginActivity::class.java))

                    }

                }catch (e:Exception){
                    e.printStackTrace()
                }
            }
        }
        background.start()
    }
    private fun canToggleGPS(): Boolean {
        val pacman = packageManager
        var pacInfo: PackageInfo? = null

        try {
            pacInfo = pacman.getPackageInfo("com.android.settings", PackageManager.GET_RECEIVERS)
        } catch (e: PackageManager.NameNotFoundException) {
            return false //package not found
        }

        if (pacInfo != null) {
            for (actInfo in pacInfo.receivers) {
                //test if recevier is exported. if so, we can toggle GPS.
                if (actInfo.name == "com.android.settings.widget.SettingsAppWidgetProvider" && actInfo.exported) {
                    Log.d("gps toggle info","true")
                }
            }
        }
        Log.d("gps toggle info","true")
        return false //default
    }

    private fun turnGPSOn() {
        val provider = Settings.Secure.getString(contentResolver, Settings.Secure.LOCATION_PROVIDERS_ALLOWED)

        if (!provider.contains("gps")) { //if gps is disabled
            val poke = Intent()
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider")
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE)
            poke.data = Uri.parse("3")
            sendBroadcast(poke)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == AppConstants.GPS_REQUEST) {
                isGPS = true // flag maintain before get location
            }
        }
    }
}
